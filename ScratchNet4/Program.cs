﻿using MdfTools.V4;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Data;

namespace ScratchNet4
{
    public class ChannelDataViewModel
    {
        public string ChannelName { get; set; }
        public double[] xAxisLabel { get; set; }
        public double[] yAxisLabel { get; set; }
    }

    public class ChannelGroupMetadataViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public string MetadataId { get; set; }
        public List<ChannelMetadataInsertViewModel> Channels { get; set; }

    }

    public class ChannelMetadataInsertViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MetadataId { get; set; }
        public string ChannelGroupMetadataId { get; set; }
        public string ChannelGroupName { get; set; }
        public string Unit { get; set; }
        public string ExtraJsonProperties { get; set; }
    }

    public class TestDataMetadataViewModel
    {
        public Guid Id { get; set; }
        public string OtherProperties { get; set; }
        public List<ChannelGroupMetadataViewModel> ChannelGroups { get; set; }
    }

    

    public class Program
    {
        public static void Main(string[] args)
        {
            //try
            //{
                //NpgsqlConnection connection = new NpgsqlConnection("Server=localhost;Port=5432;Database=postgres;User Id=postgres;Password=admin");
                //connection.Open();

                //NpgsqlCommand cmd = new NpgsqlCommand();
                //cmd.Connection = connection;
                //cmd.CommandType = CommandType.Text;
                //cmd.CommandText = "select * from conditions";
                //NpgsqlDataAdapter nda = new NpgsqlDataAdapter(cmd);
                //DataTable dt = new DataTable();
                //nda.Fill(dt);
                //cmd.Dispose();
                //connection.Close();



            //    using (NpgsqlConnection con = getConnection())
            //    {
            //        double tt = 3.1815183620000003;
            //        string query = "insert into public.conditions(time,location,humidity)values('"+ DateTime.Now.Date.ToString("MM/dd/yyyy") +" 00:00:"+tt.ToString("N6")+"','patio', 78)";
            //        NpgsqlCommand cmd = new NpgsqlCommand(query, con);
            //        con.Open();
            //        int n = cmd.ExecuteNonQuery();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return;
            //}
            //Mdf4File.Bench("C:\\Users\\ra\\Downloads\\R5_SW32_VICM_LimpHome2021-06-08_19-03-37_jarvis.mf4");
            var mf4 = Mdf4File.Open("C:\\Users\\raf00942\\Downloads\\R5_SW32_VICM_LimpHome2021-06-08_19-03-37_jarvis.mf4"); // Provide relative path
            
            // Iterate through channel groups
            //Console.WriteLine("Total Channel Groups:" + mf4.ChannelGroups.Count);
            //var id = Guid.NewGuid();
            //foreach (var chnGrp in mf4.ChannelGroups)
            //{
            //    //Console.WriteLine(chGrp.Name);
            //    var masterChannelData = Mdf4Sampler.LoadFull(chnGrp.MasterChannel).FirstOrDefault().GetSpan<double>();
            //    var channelsWOMasterChannel = chnGrp.Channels.Where(x => x.Name != chnGrp.MasterChannel.Name).ToList();
            //    foreach (var chn in channelsWOMasterChannel)
            //    {
            //        var samples = Mdf4Sampler.LoadFull(chn).FirstOrDefault().GetSpan<double>();
            //        for (int i = 0; i < masterChannelData.Length; i++)
            //        {
            //            using (NpgsqlConnection con = getConnection())
            //            {
            //                var time = string.Format("{0:00}:{1:00}:{2:00.000000}", (int)masterChannelData[i] / 3600, (int)(masterChannelData[i] %3600) / 60, (double)masterChannelData[i] % 60);
            //                string query = "INSERT INTO public.mdfdata(testdataid, time, channelgroupname, channelname, value, unit)" +
            //                           " VALUES('" + id + "','" + DateTime.Now.Date.ToString("MM/dd/yyyy") + " " + time + "','" + chnGrp.Name + "','" + chn.Name + "'," + samples[i] + ",'" + chn.Unit + "')";

            //                NpgsqlCommand cmd = new NpgsqlCommand(query, con);
            //                con.Open();
            //                int n = cmd.ExecuteNonQuery();
            //            }
                        
            //        }
            //    }
            //}

            //Console.WriteLine("Total Channels:" + mf4.Channels.ToList().Count);
            // Iterate through all channels
            //var channles = mf4.Channels.OrderBy(x => x.Name);
            //foreach (var chn in channles)
            //{
               // Console.WriteLine("Name: " + chn.Name + "Channel Grp: " + chn.Comment);
            //}
            //var channel = new string[]{ "VICM_NMCmd_Battery", "BPCM_Pack_IsoRes", "BPCM_Pack_IsoRes_V", "BPCM_FC_IsoRes", "BPCM_FC_IsoRes_V" };

            string[,] names = new string[,] { { "VICM_NMCmd_Battery", "from CAN1 message ID=0x110" },
                { "BPCM_Pack_IsoRes", "from CAN2 message ID=0x245"},{ "BPCM_Pack_IsoRes_V", "from CAN2 message ID=0x245"} };

            //var example = mf4.Channels.Where(k => channel.Contains(k.Name)).ToArray();
            var list = new List<Mdf4Channel>();
            for (int i = 0; i< names.Length/2; i++)
            {
                list.Add(mf4.Channels.Where(x => x.Name == names[i, 0] && x.ChannelGroup.Name == names[i, 1]).FirstOrDefault());

            }

            //if (example != null)
            //{
                //var samples = Mdf4Sampler.LoadFull(list.ToArray());

                //var data = samples[0].GetSpan<double>();
                //var time = samples[1].GetSpan<double>();

                // pass 1 chanel & it's master
                List<double[]> time = new List<double[]>();
                List<double[]> arrayList = new List<double[]>();
                List<Dictionary<double, double>> timeChnValueDict = new List<Dictionary<double, double>>();
                
                foreach (var chn in list)
                {
                    var channelData = Mdf4Sampler.LoadFull(chn, chn.Master);

                    var chndata = channelData[0].GetSpan<double>().ToArray();
                    arrayList.Add(chndata);
                    var timedata = channelData[1].GetSpan<double>().ToArray();
                    time.Add(timedata);
                    Dictionary<double, double> timeChnValue = new Dictionary<double, double>();
                    for (int x = 0; x < timedata.Length; x++)
                    {
                        timeChnValue.Add(timedata[x], chndata[x]);
                    }
                    timeChnValueDict.Add(timeChnValue);
                }

                //var timeOrdered = time[0].Union(time[1]).Union(time[2]).OrderBy(a => a).ToList();
            var timeOrdered = time[0].ToList();
            for (int z = 1; z < timeChnValueDict.Count; z++)
            {
                timeOrdered.Union(time[z]);
            }
            timeOrdered = timeOrdered.OrderBy(a => a).ToList();
                var testdataid = Guid.NewGuid();
                double[] lastFoundItems = new double[names.Length / 2];
                int dictCount = 0;
                foreach(var dict in timeChnValueDict)
                {
                    lastFoundItems[dictCount] =  dict.Values.First();
                    dictCount++;
                }

                for (int i = 0; i < timeOrdered.Count; i++)
                {
                    int count = 0;
                    //double? chn1Value = null;
                    //double? chn2Value = null;
                    //double? chn3Value = null;
                // Fixed array of 25
                    double[] chnValues = new double[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };
                    foreach (var chnData in timeChnValueDict)
                    {
                        double? value = null;
                        if(chnData.ContainsKey(timeOrdered[i]))
                        {
                            value = chnData[timeOrdered[i]];
                            lastFoundItems[count] = value.Value;
                        }
                        else
                        {
                            value = lastFoundItems[count];
                        }
                    chnValues[count] = value.Value;
                        //if (count == 0)
                        //    chn1Value = value;
                        //else if (count == 1)
                        //    chn2Value = value;
                        //else if (count == 2)
                        //    chn3Value = value;
                        count++;
                        
                    }
                    using (NpgsqlConnection con = getConnection())
                    {
                        var timeeee = string.Format("{0:00}:{1:00}:{2:00.000000}", (int)timeOrdered[i] / 3600, (int)(timeOrdered[i] % 3600) / 60, (double)timeOrdered[i] % 60);
                        string query = "INSERT INTO public.mdfdata(testdataid, configurationid, time, channel1, channel2, channel3)" +
                                       " VALUES('" + testdataid + "', '1' ,'" + DateTime.Now.Date.ToString("MM/dd/yyyy") + " " + timeeee + "'," + chnValues[0] + "," + chnValues[1] + "," + chnValues[2] + ")";
                        NpgsqlCommand cmd = new NpgsqlCommand(query, con);
                        con.Open();
                        int n = cmd.ExecuteNonQuery();
                    }
                }





            //}

            // To get channel data, iterate through all channels, get Mdf4Sampler and exctract data.
            //var allChannelSamples = Mdf4Sampler.LoadFull(mf4.Channels);

            // use the samples for something.
            //    foreach (var sample in allChannelSamples)
            //    {
            //        var data = sample.GetSpan<double>();
            //    }
            GC.Collect();
            mf4.Dispose();
        }

        public static NpgsqlConnection getConnection()
        {
            return new NpgsqlConnection("Server=localhost;Port=5432;Database=postgres;User Id=postgres;Password=admin");
        }

        public static void getChannelData(Mdf4File file, List<string> channels, string timeChannel)
        {
            var example = file.Channels.Where(a => channels.Any(x => x.ToLower() == a.Name.ToLower()));
            List<ChannelDataViewModel> result = new List<ChannelDataViewModel>();

            var samples = Mdf4Sampler.LoadFull(example);
            
            var time = samples.FirstOrDefault(x => x.Channel.Name.ToLower() == timeChannel.ToLower()).GetSpan<double>().ToArray();
            foreach (var sample in samples)
            {
                var chnData = sample.GetSpan<double>().ToArray();
                var chnDataVm = new ChannelDataViewModel()
                {
                    ChannelName = sample.Channel.Name,
                    xAxisLabel = time,
                    yAxisLabel = chnData
                };
                result.Add(chnDataVm);
            }            
        }

        public static void getChannelDataByChannelGroup(Mdf4File file, List<string> channelGroups, string timeChannel)
        {
            var chnGroups = file.ChannelGroups.Where(a => channelGroups.Any(x => x.ToLower() == a.Name.ToLower()));
            List<ChannelDataViewModel> result = new List<ChannelDataViewModel>();
            foreach (var grp in chnGroups)
            {
                var channels = grp.Channels.ToList();
                
                var samples = Mdf4Sampler.LoadFull(channels);
                var time = samples.FirstOrDefault(x => x.Channel.Name.ToLower() == timeChannel.ToLower()).GetSpan<double>().ToArray();
                foreach (var sample in samples)
                {
                    var chnData = sample.GetSpan<double>().ToArray();
                    var chnDataVm = new ChannelDataViewModel()
                    {
                        ChannelName = sample.Channel.Name,
                        xAxisLabel = time,
                        yAxisLabel = chnData
                    };
                    result.Add(chnDataVm);
                }                
            }
        }

        public static void getChannelData(Mdf4File file, List<string> channelGroups, List<string> channels, string timeChannel)
        {

            var chnGroups = file.ChannelGroups.Where(a => channelGroups.Any(x => x.ToLower() == a.Name.ToLower())).ToList();
            
            List<ChannelDataViewModel> result = new List<ChannelDataViewModel>();

            var chns = chnGroups.Select(x => x.Channels.Where(a => channels.Any(b => b.ToLower() == a.Name.ToLower())).FirstOrDefault());

            var samples = Mdf4Sampler.LoadFull(chns);

            var time = samples.FirstOrDefault(x => x.Channel.Name.ToLower() == timeChannel.ToLower()).GetSpan<double>().ToArray();
            foreach (var sample in samples)
            {
                var chnData = sample.GetSpan<double>().ToArray();
                var chnDataVm = new ChannelDataViewModel()
                {
                    ChannelName = sample.Channel.Name,
                    xAxisLabel = time,
                    yAxisLabel = chnData
                };
                result.Add(chnDataVm);
            }
        }
    }
}
